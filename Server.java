
package rmi;

import rmitools.EndgameResult;
import rmitools.RMIMove;
import rmitools.TurnsHandler;
import rmitools.IDGenerator;
import java.awt.event.ActionEvent;
import java.rmi.AlreadyBoundException;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;
import javax.swing.AbstractAction;

/**
 * Server
 * the server is the anchor between all the clients,
 * its like a star diagram.
 * @see more information in the project documentation.
 * @author Niv B.
 */
public class Server extends UnicastRemoteObject implements ServerInterface {
    
    private static final String RMI_SERVER_NAME = "server"; // the name we'll give to the server in the registry
    private static int RMI_PORT = 1099; // default rmi port, but i write it here anyway
    
    private static IDGenerator id_generator = new IDGenerator(5000, 1234); // to generate unique and random IDs for the clients of this server
    
    
    List<ClientInterface> clients; // clients list the server handles
    ServerForm form; // the jframe that is displayed for the manager of the server, giving him some functionality
    Registry registry; // reference to the RMI registry used by the server
    
    List<TurnsHandler> matchups; // each match is represented by a TurnsHandler, that handles the game between the players
    
    /**
     * Creates a server that uses the 1099 port always, so 2 servers on same PC is not possible;
     * open an RMI registry and exporting the server.
     * 
     * @throws RemoteException
     * @throws AlreadyBoundException 
     */
    public Server() throws RemoteException, AlreadyBoundException {
        super();        
        clients = new ArrayList<>();
        matchups = new ArrayList<>();
        form = new ServerForm();
        registry = LocateRegistry.createRegistry(RMI_PORT);
        registry.bind(RMI_SERVER_NAME, this); // we'll call the object in the registry "server" (very creative)
        form.setTitle("Server at port "+ RMI_PORT);
        System.err.println("Server ready");        
        form.setVisible(true);
        CreateButtonsClicks();
    }    
    
    /**
     * remove the server from the RMI
     * @throws NoSuchObjectException
     * @throws RemoteException
     * @throws NotBoundException 
     */
    public void CloseServer() throws NoSuchObjectException, RemoteException, NotBoundException{        
        registry.unbind(RMI_SERVER_NAME);
        UnicastRemoteObject.unexportObject(this, true);
    }

    
    // <editor-fold desc="RMI methods (called by clients)">
    
    
    @Override
    public Integer AddClient(ClientInterface client) throws RemoteException {
        clients.add(client);
        return id_generator.next();
    }

    @Override
    public void GetClientText(String text) throws RemoteException {
        // showing it on the server chat
        form.jTextArea1.setText(form.jTextArea1.getText()+text+"\n"); // adding the text, and a new line in the server chat
        // optional, sending it to all the other clients. this is purely for fun
        for (ClientInterface client : clients) {
            client.Update(text);
        }
    }

    @Override
    public String ServerInfo() throws RemoteException {
        return form.jTextField_serverStatus.getText();
    }
    
    
    @Override
    public void CloseClientConnection(int clientID, ArrayList<Integer> gamesID) throws RemoteException {
        // search the client from clients list      
        ClientInterface client_to_close = null;
        for (ClientInterface client : clients) {
            if(client.GetClientID() == clientID) {
                client_to_close = client;
                break;
            }
        }
        if(client_to_close == null) { // if we didnt find the client in the clients list
            System.err.println("Trying to close a client that doesnt exists");
            return;
        }
        // finding all the games connections this client has
        ArrayList<TurnsHandler> matches_to_close = new ArrayList<>();
        for (Integer gameID : gamesID) { // for all the games id we got
            for (TurnsHandler matchup : matchups) { // searching the matchup with this game id
                if(matchup.getGameInstanceID() == gameID) {
                    matches_to_close.add(matchup);
                }
            }
        }
        // removing the closed matches from the matches array
        for (TurnsHandler match : matches_to_close) {
            // finishing the matchup game for all the relevant clients
            ClientInterface[] clients = match.getPlayers();
            for (ClientInterface client : clients) {
                if(client.GetClientID() == clientID) // the client itself will finish its games because he was the one asking for it
                    continue;
                try {
                    client.FinishGame(match.getGameInstanceID(), "Game finished by remote user.");
                } catch (RemoteException ex) {
                    // if we cant reach the client we dont care, it means connection already ended
                }
            }
            matchups.remove(match);
        }
        // remove the client itself from clients list
        clients.remove(client_to_close);
    }
        
 
    @Override
    public void gameCreateRequest(String game_name, int clientID) throws RemoteException {
        //@NOTE(amount of players) : we are assuming each game being started will require a constant amount of players. if a different amount of players requires, then we'll need to create a new game entiarly or change some of the TurnsHandler methods, and add a remote method for the server, that the client will invoke in order to change the amount of players
        // first search the client in our "database"
        for (ClientInterface client : clients) {
            if(client.GetClientID() == clientID) {
                // search if thers an open game already (same game as game_name of course)
                for (TurnsHandler th : matchups) {
                    if(th.getGameName().equals(game_name) && !th.isPlayerExists(clientID)) { // we found a game like we wanted, and we are not already in this game
                        if(th.addPlayer(client)) { // successfully added the client as a player
                            int counter = 1; // counter to represent the player that we are adding (in our case its always 1 and 2, because we have only 2-player games)
                            for (ClientInterface game_client : th.getPlayers()) { 
                                game_client.Update("Players found! Game starting."); // telling players they can start playing
                                game_client.StartGame(th.getGameInstanceID(), game_name, counter++); // telling client to start a given game. counter used for player identification (ex: i can make it random from [1,2] so each player will randomly play white or black. atm its by who asked for a game first)
                                game_client.InputPermission(th.getGameInstanceID(), true); // allow games to make moves       
                            }
                            return ;
                        }
                    }
                }
                // else, we didnt find a game that we can join, so creating a new one
                ClientInterface[] players = new ClientInterface[2]; // NOTE: atm only 2 players are allowed, but if 3 players or more i should have some hashmap to indicate game name and how many players allowed
                TurnsHandler th = new TurnsHandler(game_name, players); // parameter will change according to game_name
                th.addPlayer(client);
                matchups.add(th);
                client.Update("Searching for players...");
                return ;
            }
        }
        // else, we didnt find the client in our "database" and that .. shouldnt happen
        System.err.println("client is not recognized by the server!");
    }


    @Override
    public boolean gameMoveRequest(RMIMove move, int clientID, int gameID) throws RemoteException {
        TurnsHandler matchup_to_remove = null;
        // we wont check if move is legal atm, only checking if its the right turn
        for (TurnsHandler th : matchups) {
            if(th.getGameInstanceID() == gameID) { // we found the given game
                if(move.isPlayerResign()) {
                    EndgameResult endgame = th.radiateMove(gameID, move);
                    for (ClientInterface game_client : th.getPlayers()) {  // disallow all the clients in this game to make any additional move - game ended
                        game_client.InputPermission(th.getGameInstanceID(), false);
                    }
                    th.radiateEndgame(th.getGameInstanceID(), endgame);
                    matchup_to_remove = th;
                    break;
                }
                // else, no one resign, game continue as usual
                if(th.getCurrentID() == clientID) {
                    th.nextTurn(); // shift turn to the next player
                    EndgameResult endgame = th.radiateMove(gameID, move);
                    // check if the move made a win
                    if(endgame != null) {
                        for (ClientInterface game_client : th.getPlayers()) {  // disallow all the clients in this game to make any additional move - game ended
                            game_client.InputPermission(th.getGameInstanceID(), false);
                        }
                        th.radiateEndgame(th.getGameInstanceID(), endgame);
                        matchup_to_remove = th; // telling client that the move has been made
                    }
                    return true;
                }
            }
        }
        if(matchup_to_remove != null) { // if game has ended, we want to remove it from the list
            matchups.remove(matchup_to_remove);
            return true;
        }
        return false;
    }
    
    
//####################################################################### </editor-fold>   
    
    // <editor-fold desc="Creation of Buttons">
    
    
    /**
     * creating all the buttons functionality (when clicking on them)
     */
    private void CreateButtonsClicks() { 
        
        form.jButton_callclient.addActionListener(new AbstractAction() {
            // Call Client button to put information of the clients and matches occuring
            @Override
            public void actionPerformed(ActionEvent e) {
                StringBuilder buffer = new StringBuilder();
                buffer.append(form.jTextArea1.getText());
                for (ClientInterface client : clients) {
                    try {
                        int clientID = client.GetClientID();
                        boolean first_match_in_array = true; // for aesthetic printing
                        buffer.append(clientID);
                        for (TurnsHandler matchup : matchups) {
                            if(first_match_in_array) {
                                buffer.append(": ");
                                first_match_in_array = false;
                            }
                            if(matchup.isPlayerExists(clientID)) {
                                buffer.append(matchup.getGameInstanceID()+"..");
                            }
                        }
                    } catch (RemoteException ex) {
                        buffer.append("client unreachable"); // can only be tested if power surge happen in client..
                    }
                    buffer.append("");
                    buffer.append("\n");
                }
                form.jTextArea1.setText(buffer.toString());
            }
        });
        
        form.jButton_send.addActionListener(new AbstractAction() {
            // Send to client the text in TextField of the server
            @Override
            public void actionPerformed(ActionEvent e) {
                String output = form.jTextField_serverStatus.getText();
                for (ClientInterface client : clients) {
                    try {
                        client.Update("SERVER: " + output);
                    } catch (RemoteException ex) {
                        // if we cant reach a client an expection will be throwned; might happen if client had power surge or w/e, in any case we'll just skip it
                    }
                }
            }
        });
        
        form.jButton_exit.addActionListener(new AbstractAction() {
            // close the communication
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    CloseServer();
                    form.dispose();                    
                } catch (RemoteException | NotBoundException ex) {
                    //Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
                    System.err.println("Server was not able to find itself in the registry. Closing could'nt occure.");
                }
            }
        });
    }
    //####################################################################### </editor-fold>   

}