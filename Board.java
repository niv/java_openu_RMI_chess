
package tools;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import javax.swing.JPanel;

/**
 * class Board is a JPanel that handles drawing of a board that is made of Cells,
 * can be extended by the games created.
 * @author Niv B.
 */
public class Board extends JPanel {
    
    public static final int SDIM = 50; // square dimension
    public static final int SPACING = 5; // spacing between squares
    
    protected int xSTART; // the start of the board x (used to create the board initialy)
    protected int ySTART; // the start of the board y (used to create the board initialy)
    
    protected Cell[][] board; // the board itself, consisting of matrix of cells
    protected int dimx; // dimension over x
    protected int dimy; // dimension over y
   
    protected Color background_color; // background color of the board
    
    /*========== ctor methods ==========*/
    
    /**
     * Creates a new instance of board.
     * @param dimensionX x dimension
     * @param dimensionY y dimension
     * @param start_x starting x of board
     * @param start_y starting y of board
     * @param background_color the color of the background of the board
     */
    public Board(int dimensionX, int dimensionY, int start_x, int start_y, Color background_color){
        super();
        this.background_color = background_color;
        xSTART = start_x;
        ySTART = start_y;
        ResetBoardDimensions(dimensionX, dimensionY);       
        addMouseListener(Mouse.Instance());
    }    
    
    /*========== get/set methods ==========*/
    public Cell getAt(int i, int j){ return board[i][j]; }    
    public int getDimX(){ return dimx; }
    public int getDimY(){ return dimy; }
    
    /*========== object methods ==========*/
    
    /**
     * changing the size of the board
     * @param dimx the new x dimension
     * @param dimy the new y dimension
     */
    public final void ResetBoardDimensions(int dimx, int dimy) {
        this.dimx = dimx;
        this.dimy = dimy;
        board = new Cell[dimx][dimy];
        for (int i = 0; i < dimx; i++) {
            for (int j = 0; j < dimy; j++) {
                int x = xSTART + i*(SDIM + SPACING);
                int y = ySTART + j*(SDIM + SPACING);
                board[i][j] = new Cell(x,y,SDIM,SDIM);
            }
        }
    }
    
    /**
     * an option for a mouse interaction,
     * should be overrided by games that implement the board.
     */
    public void onMouseRelease(MouseEvent me) {
    }
    
    
    /**
     * an option for a mouse interaction,
     * should be overrided by games that implement the board.
     */
    public void onMousePressed(MouseEvent me) {
    }
    
    /**
     * painting the board on the graphical component
     * @param g the graphic component
     */
    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g); /* apperantly its VERY important to include the super function */
   
        // drawing board background
        if(background_color != null) {
            g.setColor(background_color);
            g.fillRect(
                    xSTART-SPACING,
                    ySTART-SPACING,
                    dimx *(SDIM + SPACING)+SPACING,
                    dimy *(SDIM + SPACING)+SPACING);
        }
        
        // drawing the board itself
        for (Cell[] cells : board) {
            for (Cell cell : cells) {
                cell.DrawCell(g);
            }
        }
    }
    
    
}
