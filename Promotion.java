
package games.chess.specialmoves;

import java.awt.Component;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import games.chess.LogicChessBoard;
import games.chess.Move;
import games.chess.chesspieces.ChessPiece;
import games.chess.chesspieces.PiecePawn;

/**
 * class Promotion
 * acts like a static class (singleton) but we dont really have any variables for this class,
 * so its only been used for the static methods that are used to produce and calculate
 * the promotion scenario;
 * this is the same as the math library, which is virtually static.
 * @author Niv B.
 */
public class Promotion {
    
    /***************************************************************************/

    // <editor-fold desc="class methods">
    
    /**
     * @return an array of strings, each string starts with the representing letter of a piece that can be promoted from a pawn
     */
    public static String[] getPromotionPossibilities() {
        return new String[]{"Q - Queen", "R - Rook", "B - Bishop", "N - Knight"};
    }
    
    
    /**
     * assuming given piece is a pawn (at a normal chess game pawn is the only one that can be promoted, so i didn't make a generic function in ChessPiece but rather a variable in PiecePawn to indicate it)
     * @param piece
     * @return 
     */
    public static char getPawnPromotionLetter(ChessPiece piece) {
        return ((PiecePawn)piece).getPromotionLetter();
    }
    


    /**
     * make a popup messege allowing the user to select which chess piece
     * he wants the pawn to advance to;
     * the getPromotionPossibilities function is used to show the options,
     * and the first letter in them is the first letter of the promoted chess piece.
     * if given piece is not a pawn, this function will do nothing.
     * @param parent the component (board) that will be the parent of this popup
     * @param piece the pawn to be promoted. note that its not explicitly a pawn because i didnt want the board to use any of the chess piece classes other than ChessPiece.
     */
    public static void promotionPopupUpdate(Component parent, ChessPiece piece) {
        try {
            PiecePawn pawn = (PiecePawn)piece;
            String selection = (String)JOptionPane.showInputDialog(
                                parent,
                                "Choose promotion possibility:",
                                "Promotion",
                                JOptionPane.PLAIN_MESSAGE,
                                new ImageIcon(ChessPiece.getImagePiece("pawn", piece.isWhite())),
                                Promotion.getPromotionPossibilities(),
                                null);
            pawn.setPromotionLetter(selection.charAt(0));
        } catch (Exception e) {
            // do nothing
        }
    }
    
    /**
     * Check promotion legality only;
     * assuming the move already occured
     * @param board
     * @param move
     * @return true if given move is legal by the rules of promotion in chess, false otherwise
     */
    public static boolean isPromotionMove(LogicChessBoard board, Move move) {
        if(move.piece.getLetter() != 'P') // if not a pawn, promotion unavailable
            return false;
        int forwardY = move.yto+(move.piece.isWhite()?+1:-1);
        if(board.isInBounds(move.xto, forwardY)) // if we can move forward then we are not on the last tile, promotion unavailable
            return false;
        return true;
    }
    
    /**
     * updating given move to be a promotion move
     * @param board 
     * @param move 
     * @param promotion the piece type ('K','Q','R','N','B') to be promoted to. 
     */
    public static void updatePromotionMove(LogicChessBoard board, Move move, char promotion) {
        ChessPiece pawn = move.piece;
        move.promoted = pawn;
        // removing the pawn from the white pieces array
        board.removePiece(pawn.getX(), pawn.getY());
        //move.piece = new PieceQueen(pawn.isWhite(), move.xto, move.yto);
        // updating pieces array to include the new promoted piece, removing the old one
        int prev_y = pawn.getY();
        board.addPiece(move.piece.isWhite(), promotion, move.xto, prev_y);
        // the move piece changing to the promoted piece instead of the pawn
        move.piece = board.getPieceChess(move.xto, prev_y);
        // check for check/mate
        if(Checkmate.isKingSafe(board, !pawn.isWhite())) { // checking if enemy king is safe (ex: if we promoted into a Queen that can now check the enemy). note we dont need to check OUR king because the promotion cant possibly make it more safe, only the move that it did, and it was checked before we use this function
            move.check = false; 
        } else {
            move.check = true;
        }
    }
    
            
   /**
    * undo promotion in the move parameters and, if a promotion exists
    * @param board 
    * @param move 
    */
    public static void undoPromotion(LogicChessBoard board, Move move) {
        if(move.promoted==null)
            return ;
        // changing back the piece to be the promoted pawn
        move.piece = move.promoted;
        // removing the promotion indication
        move.promoted = null;
        // removing the piece from the logical board
        board.removePiece(move.xto, move.yto);
        // re-add the pawn to the logical board
        board.updatePiecesArray(move.piece, -1);
    }
    
    
    // </editor-fold>

}
