
package games.chess.chesspieces;

import games.chess.LogicChessBoard;
import games.chess.MovesList;

/**
 * class PieceKnight
 * knight has a special kinf of move which he can also capture with,
 * two steps in one non-diagonal direction,
 * and then one step in orthogonal direction of the previous ones
 * @author Niv B.
 */
public class PieceKnight extends ChessPiece {


    /***************************************************************************/

    // <editor-fold desc="ctor">
    
    /**
     * ctor creates a new knight chess piece
     */
    public PieceKnight(boolean is_white, int i, int j) {
        super(is_white,i,j);
        img = getImagePiece("knight", is_white);
    }
    
    // </editor-fold>


    /***************************************************************************/

    // <editor-fold desc="class methods">
    
    @Override
    public MovesList generateMoves(LogicChessBoard b) {
        int[][] pool = new int[][]{ 
            {x+2,y+1},{x+2,y-1},
            {x-2,y+1},{x-2,y-1},
            {x+1,y+2},{x+1,y-2},
            {x-1,y+2},{x-1,y-2} };
        return generateMoves(b, pool);
    }
    
    @Override
    protected boolean isLegalSquare(int xto, int yto) {
        int dx = xto-x;
        int dy = yto-y;
        int dx2 = dx*dx;
        int dy2 = dy*dy;
        if(dx2+dy2 == 5) // think about it
            return true;
        return false;
    }
    
    @Override
    public e_Target isTargetBlocked(LogicChessBoard b, int xto, int yto) {
        // with the knight its easy because he can jump over pieces, so he only get blocked if destination is blocked
        ChessPiece cp = b.getPieceChess(xto, yto);
        if(cp==null)
            return e_Target.OPEN;
        if(this.isFriendly(cp))
            return e_Target.BLOCKED_FRIENDLY;
        return e_Target.CAPTURE_MOVE;            
    }
    
    @Override
    public char getLetter() { return 'N';  }
    
    @Override
    public boolean isLongRange() { return false; }
    
    
    // </editor-fold>

}
