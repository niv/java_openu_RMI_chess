
package rmi;

import rmitools.GameFactory;
import rmitools.EndgameResult;
import rmitools.RMIMove;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.AccessException;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.swing.AbstractAction;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * class Client
 * the client is what the user see,
 * allowing him to dis/connect to server, chat, and play games
 * @author Niv B.
 */
@SuppressWarnings("LeakingThisInConstructor")
public class Client extends ClientForm implements ClientInterface {
    
    // window constants
    private final static int WIN_XSTART = 200; // starting X of window
    private final static int WIN_YSTART = 80; // starting Y of window
    private final static int WIN_DELTA = 50; // in case more than 1 client produced in the same program, we shift their positions (mainly for debuging)
    
    // msg constants
    private final static String MSG_SERVER_DOWN = "Server is down due to maintence. We apologise for any inconvinient occured.";
            
    // RMI constants
    private final static int RMI_PORT = 1099; // default rmi port
    private final static String RMI_SERVER_NAME = "server"; // the name we will look for in the RMI registry, in order to retrieve the reference to the server
    
    private static int _clients_count = 0; // more for debuging purpose, so i can put different clients in different positions
    
    private int id; // client id, unique to each client
    private ServerInterface server; // reference to the server which the client connected to
    
    private List<VisualGame> active_games; // all the games this client has
    
    
    /**
     * Initialize a new instance of this the client;
     * starting with the server connection window.
     * 
     * @throws RemoteException
     * @throws NotBoundException 
     */
    public Client() throws RemoteException, NotBoundException {  
        super();
        active_games = new ArrayList<>();
        // set client window location
        this.setLocation(WIN_XSTART+WIN_DELTA*_clients_count, WIN_YSTART+WIN_DELTA*_clients_count);
        
        // add games options to the combo box selection
        for (String string : GameFactory.getGamesName()) {
            jComboBox_game_selection.addItem(string);
        }
        
        // implement buttons actions     
        CreateButtonsClicks(); 
        
        // set the UI to be server selection
        setUI(true);
                
        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setVisible(true);
        _clients_count++;
        // repaint and validate just to make sure we can see everything
        repaint();
        validate();
    }
    
    /**
     * @return the text in the text field of the user input box
     */
    public String getTextFieldText() {
        return this.jTextField_chatInput.getText();
    }    
    
    /**
     * this can be used while not using the RMI.
     * @return the client id
     */
    public int getClientID() {
        return id;
    }
    
    /**
     * when the player makes a move, it will request his client to validate the move with the server;
     * if the validation will be succesfull, the server will call the two clients update method and
     * each of the clients will call its own game update function
     * @param gameID specific game we are making the move on
     * @param move 
     */
    public void requestMove(int gameID, RMIMove move) {
        try {
            if( !server.gameMoveRequest(move, this.id, gameID) ) { // telling the server to make a move; if return false then the server didnt made the move
                System.err.println("Server denied your move!");
            }
            // else, the move will be made
        } catch (RemoteException ex) {
            System.err.println(MSG_SERVER_DOWN);
        }
    }
    
    
    ///////// RMI client-related override methods ///////////
    
    @Override
    public int GetClientID() throws RemoteException {
        return id;
    }
    

    @Override
    public void Update(String val) throws RemoteException {
        jTextArea_chat.append(val+"\n");
    }
    
    ///////// RMI game-related override methods ///////////
    
    @Override
    public void StartGame(int gameID, String game_name, int player) throws RemoteException {
        IGame game_board = GameFactory.getGame(game_name, player); // creating a new game from the game factory
        // adding the game to the client active games list
        active_games.add(new VisualGame(
                    this, 
                    this.getX()+this.getWidth(), this.getY(),
                    gameID,
                    game_board));
    }
    

    @Override
    public EndgameResult MakeMove(int gameID, RMIMove move, String msg) throws RemoteException {
        for (VisualGame game : active_games) { // finding the game given the id
            if(game.getGameID()==gameID) {
                if(msg!=null) { // if we have a messege to give, we show it
                    game.setGameStateText(msg);
                }
                EndgameResult endgame = game.makeMove(move);
                if(endgame != null) { // making the move in that game
                    return endgame; // return true if we have endgame
                }
            }
        }
        return null;
    }
    
    
    @Override
    public void InputPermission(int gameID, boolean allow_input) throws RemoteException {
        for (VisualGame game : active_games) { // finding the game given the id
            if(game.getGameID()==gameID) {
                game.setInputPermission(allow_input);
            }
        }
    }
    
    
    
    @Override
    public void FinishGame(int gameID, String msg) throws RemoteException {
        for (VisualGame game : active_games) { // finding the game given the id
            if(game.getGameID()==gameID) {
                game.finish(msg);
            }
        }
    }
    
    
    /**
     * change the UI that is displayed in the client jframe;
     * server UI : user will select a server and port to connect to
     * client UI : the acctual client UI with game selection and chat
     * @param server_selection true if to display the server selection UI, false otherwise
     */
    private void setUI(boolean server_selection) {
        // set server selection UI to client managment UI
        String title_text = "Client disconnected";
        int delta_width = 30;
        int delta_height = 24+50;
        JPanel selected_panel;
        JPanel hidden_panel;
        if(server_selection) {
            selected_panel = jPanel_connectToServer;
            hidden_panel = jPanel_clientPanel;
            
        } else {
            selected_panel = jPanel_clientPanel;
            hidden_panel = jPanel_connectToServer; 
            title_text = "Client #"+this.id;
        }
        // changing visible panel
        hidden_panel.setVisible(false);
        selected_panel.setVisible(true);
        Dimension dim = selected_panel.getPreferredSize();
        // adjusting size of the jframe size and jpanel position on the jframe
        setLayout(null);
        selected_panel.setLocation(10, 10);
        this.setSize(dim.width+delta_width, dim.height+delta_height);
        // setting default text in containers
        this.setTitle(title_text);
        this.jTextArea_chat.setText("<<< This is the chat >>>\n");
        this.jTextField_chatInput.setText("<enter text here>");
        // repainting and validating because windows can be annoying and not show it sometimes
        selected_panel.repaint();
        selected_panel.validate();
    }
    
    
    /**
     * try to connect this client to the server registry at given ip,
     * using given port for exporting the objects;
     * assuming the server itself using the default RMI port.
     * @param ip null or "localhost" for localhost, or ip address
     * @param port null for default RMI port (1099), or port number otherwise
     * @return true if connection passed, false otherwise
     */
    public boolean tryToConnect(String ip, Integer port) {
        boolean connection_failed = false;
        try {
            // exporting self using RMI procedure
            if(port == null) {
                UnicastRemoteObject.exportObject(this, RMI_PORT);
            } else {
                UnicastRemoteObject.exportObject(this, port);
            }
            // get server from registry
            if(ip == null || ip.equals("localhost")) {
                server = (ServerInterface)LocateRegistry.getRegistry().lookup(RMI_SERVER_NAME);
            } else {
                server = (ServerInterface)LocateRegistry.getRegistry(ip).lookup(RMI_SERVER_NAME);
            }
            // send self to server, getting an id
            this.id = server.AddClient(this);
        } catch (NotBoundException ex) {
            System.out.println("Can't find the server.");
            connection_failed = true;
        } catch (AccessException ex) {
            System.out.println("Access denied from the RMI.");
            connection_failed = true;
        } catch (RemoteException ex) {
            //Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("Connection to the server failed.");
            connection_failed = true;
        }
        if(connection_failed) {            
            try {
                UnicastRemoteObject.unexportObject(this, true);
                LocateRegistry.getRegistry().unbind(RMI_SERVER_NAME);
            } catch (Exception ex) {
                /* will only throw if the object wasnt exported, but this cant be true,
                   because we only get here if connection_failed==true,
                   and this will only happen if we made the first line in the first try{}catch{}
                   which is UnicastRemoteObject.exportObject ...
                   so we will NEVER get here. c'est la vie, java cant know this.
                */
            }
            return false;
        }
        setUI(false);
        System.out.println("Connection successfull");
        return true;
    }
    
    /**
     * same as tryToConnect/1, but in here we recieve the ip and port from the
     * text the user entered in the JTextField inside the server jpanel
     * @return true if connection passed, false otherwise
     */
    public boolean tryToConnect() {
        String ip = this.jTextField_serverIP.getText();
        String port_string = this.jTextField_serverPort.getText();
        int port = port_string.equals("") ? null : Integer.parseInt(port_string);
        return tryToConnect(ip, port);
    }


    /**
     * creating all the buttons functionality (when clicking on them)
     */
    private void CreateButtonsClicks() {
        
        // menu buttons
        /********************************************/
        
        jMenuItem_Exit.addActionListener(new AbstractAction() {
            // menu exit
            private Client ref_client;
            @Override
            public void actionPerformed(ActionEvent e) {
                ref_client.dispose(); // similar to clicking on "X"
            }
            // using a trick here - when creating the anonymous class we invoke initialize/1 method to initialize private variable that will be the reference to the client that invoked the thread. i believe there is another option to find it like using the ActionEven parameter and getting back the invoker, but i like this trick more - its a java trick and not a swing trick
            private ActionListener initialize(Client client) {
                ref_client = client;
                return this; // important to return the action listener because we add it to the button action
            }
        }.initialize(this));
        
        
        jMenuItem_servermenuDisconnect.addActionListener(new AbstractAction() {
            // menu server button - connect to different IP/port
            private Client ref_client;
            @Override
            public void actionPerformed(ActionEvent e) {
                ref_client.disconnect();
                ref_client.setUI(true);
            }
            // using a trick here - when creating the anonymous class we invoke initialize/1 method to initialize private variable that will be the reference to the client that invoked the thread. i believe there is another option to find it like using the ActionEven parameter and getting back the invoker, but i like this trick more - its a java trick and not a swing trick
            private ActionListener initialize(Client client) {
                ref_client = client;
                return this; // important to return the action listener because we add it to the button action
            }
        }.initialize(this));
        
        
        // normal buttons - server jpanel
        /********************************************/
        jButton_connectToServer.addActionListener(new AbstractAction() {
            // button 'find match' with game given in combo box selection
            private Client ref_client;
            @Override
            public void actionPerformed(ActionEvent e) {
                ref_client.tryToConnect();
            }
            // using a trick here - when creating the anonymous class we invoke initialize/1 method to initialize private variable that will be the reference to the client that invoked the thread. i believe there is another option to find it like using the ActionEven parameter and getting back the invoker, but i like this trick more - its a java trick and not a swing trick
            private ActionListener initialize(Client client) {
                ref_client = client;
                return this; // important to return the action listener because we add it to the button action
            }
        }.initialize(this));
        
        
        // normal buttons - client jpanel
        /********************************************/
        jButton_find_match.addActionListener(new AbstractAction() {
            // button 'find match' with game given in combo box selection
            private Client ref_client;
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String game_name = jComboBox_game_selection.getSelectedItem().toString();
                    server.gameCreateRequest(game_name, id);
                } catch (RemoteException ex) {
                    System.err.println(MSG_SERVER_DOWN);
                }
            }
            // using a trick here - when creating the anonymous class we invoke initialize/1 method to initialize private variable that will be the reference to the client that invoked the thread. i believe there is another option to find it like using the ActionEven parameter and getting back the invoker, but i like this trick more - its a java trick and not a swing trick
            private ActionListener initialize(Client client) {
                ref_client = client;
                return this; // important to return the action listener because we add it to the button action
            }
        }.initialize(this));

        
        this.jButton_send.addActionListener(new AbstractAction() {
            // click on 'send' button to send information to the server chat
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    server.GetClientText(id + ": " + getTextFieldText());            
                } catch (RemoteException ex) {
                    System.err.println(MSG_SERVER_DOWN);
                }
            }
        });
        this.jButton_recieve.addActionListener(new AbstractAction() {
            // click on 'recieve' button to update the text field with the server text field
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    jTextField_chatInput.setText(server.ServerInfo());
                } catch (RemoteException ex) {
                    System.err.println(MSG_SERVER_DOWN);
                }
            }
        });
        
    }
    
    /**
     * disconnect the client from the server;
     * all active games will be resigned and then disposed,
     * will use the remote method CloseClientConnection/2 of the server,
     * and unexport the client from remote method calling from the RMI protocol.
     */
    public void disconnect() {
        if(server == null) // we cant disconnect if we are not connected to a server
            return;
        // force ending all games
        ArrayList<Integer> gameID_list = new ArrayList<>();
        for (VisualGame visualGame : active_games) {
            gameID_list.add(visualGame.getGameID());
        }
        // disposing all games windows
        for (Iterator<VisualGame> it = active_games.iterator(); it.hasNext();) {
            VisualGame visualGame = it.next();
            visualGame.dispose();
        }
        // telling server to remove this client from its "database", while telling all participants of given games that the games ended
        try {
            server.CloseClientConnection(id, gameID_list);
        } catch (RemoteException ex) {
            System.err.println(MSG_SERVER_DOWN);
        }
        // unimport the client from RMI registry
        try {
            UnicastRemoteObject.unexportObject(this, true); // removing object from RMI
        } catch (NoSuchObjectException ex) {
            // object isnt exported. isnt logical, so we dont care
        }
        server = null;
    }
    
    
    /**
     * overriding the jframe dispose;
     * this method function as clicking the X button of the client, meaning:
     * 1) all games and client itself will be closed
     * 2) client will disconnect from server
     */
    @Override
    public void dispose() {
        if(server == null) { // first checking if we are connected. if we are not, simply disposing the jframe
            super.dispose();
            return;
        }
        disconnect();
        super.dispose();
    }
}
