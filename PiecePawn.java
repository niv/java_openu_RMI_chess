package games.chess.chesspieces;

import games.chess.specialmoves.EnPassant;
import games.chess.LogicChessBoard;
import games.chess.Move;
import games.chess.MovesList;

/**
 * class PiecePawn
 * pawn can move once forward or twice if its the begining of the game,
 * pawn can capture in two ways :
 * 1) diagonals once square only
 * 2) diagonal if enemy pawn moved 2 squares forward and we are in either direct left or right of it
 * @author Niv B.
 */

public class PiecePawn extends ChessPiece {

    
    // <editor-fold desc="class variables">
    
    private boolean double_move; // false if we didn't make first move, so we can do en passa'nt
    private char promotion_letter; // the letter which this pawn would be promoted to, if it will get to the situation. queen by default

    
    // </editor-fold>

    /***************************************************************************/

    // <editor-fold desc="ctor">
    
    /**
     * ctor creates a new pawn chess piece with default promotion to queen
     */
    public PiecePawn(boolean is_white, int i, int j) {
        super(is_white,i,j);
        img = getImagePiece("pawn", is_white);
        double_move = false;
        promotion_letter = 'Q';
    }

    
    // </editor-fold>


    // <editor-fold desc="get/set">
    
    /**
     * @return true if we can still do en'passant, false otherwise
     */
    public boolean isDoubleMove() {
        return double_move;
    }

    /**
     * @return the letter representing what this pawn will be promoted to, once promotion happends
     */
    public char getPromotionLetter() {
        return promotion_letter;
    }
    
    /**
     * @param double_move true if we can still do en'passant, false otherwise
     */
    public void setDoubleMove(boolean double_move) {
        this.double_move = double_move;
    }
    
    /**
     * @param promotion_letter the letter representing what this pawn will be promoted to, once promotion happends
     */
    public void setPromotionLetter(char promotion_letter) {
        this.promotion_letter = promotion_letter;
    }
    
    // </editor-fold>

    /***************************************************************************/

    // <editor-fold desc="class methods">
    
    
    @Override
    public MovesList generateMoves(LogicChessBoard b) {
        MovesList mlist = new MovesList(b.getMovesCounter());
        int direction = is_white ? +1 : -1; // (+1) for white, (-1) for black
        
        // (xto,yto) will be destinations that we will check in this method
        int xto = x;
        int yto;
        
        yto=y+1*direction;
        if(isTargetBlocked(b, x,yto)==e_Target.OPEN) { // 1 square ahead is open, required for any forward move
            // move single step ahead
            if(isLegalSquare(x,yto)) {
                mlist.add(new Move(this, x, yto));
            }
            
            // move two steps ahead
            yto=y+2*direction;
            if(isLegalSquare(x,yto) && isTargetBlocked(b, x,yto)==e_Target.OPEN) {
                // we DONT updat double_move here since we only generate the moves atm. when we make the acctual move, we'll check if delta in position y was 2 if so we'll update it
                mlist.add(new Move(this, x, yto));
            }
        }
        
        // now checking en'passant
        int[] en_passant_cell = EnPassant.getPossible(b, this);
        if(en_passant_cell != null) {
            ChessPiece enemy = b.getPieceChess(en_passant_cell[0], y); // enemy has our y, and the destination of the en'passant x
            Move move = new Move(this, en_passant_cell[0], en_passant_cell[1], enemy);
            mlist.add(move);
        }
        
        // now checking eating in diagonal        
        yto=y+1*direction;
        xto=x-1;
        if(b.isInBounds(xto, yto) && isLegalSquare(xto, yto) && isTargetBlocked(b, xto, yto)==e_Target.CAPTURE_MOVE) {
            mlist.add(new Move(this, xto, yto, b.getPieceChess(xto, yto)));
        }
        // the other diagonal
        xto=x+1;
        if(b.isInBounds(xto, yto) && isLegalSquare(xto, yto) && isTargetBlocked(b, xto, yto)==e_Target.CAPTURE_MOVE) {
            mlist.add(new Move(this, xto, yto, b.getPieceChess(xto, yto)));
        }
        
        return mlist;
    }
    
    /**
     * in here we allow the en'passant moves automaticly without checking the board;
     * that check should happen outside this class, in the same way or reverse way normal capture is checked
     * @param xto
     * @param yto
     * @return 
     */
    @Override
    protected boolean isLegalSquare(int xto, int yto) {
        int dx = xto-x;
        int dy = yto-y;
        if(is_white && dy<=0) return false; // not allowing white moving down
        if(!is_white && dy>=0) return false; // not allowing black moving up
        if(dx!=0) {             
            if((dy==1 || dy==-1) && (dx==1 || dx==-1)) // capturing (normal or en'passant, both moving in diagonal)
                return true;
        } else { // forward moves
            if(dy==1 || dy==-1 || ((dy==2 || dy==-2) && moves_count==0))
                return true;
        }
        return false;
    }
    
    @Override
    public e_Target isTargetBlocked(LogicChessBoard b, int xto, int yto) {
        ChessPiece cp = b.getPieceChess(xto, yto);
        if(cp==null)
            return e_Target.OPEN;
        if(this.isFriendly(cp))
            return e_Target.BLOCKED_FRIENDLY;
        // now its the special thing about the pawn - it IS blocked by ENEMY if we intend of moving FORWARD, so here we check it
        // note that we got here if given square is occupied, and not by a friendly piece
        // so only need to check if we are moving in diagonal or not (The only way to capture with pawn)
        if(x==xto)
            return e_Target.BLOCKED_NOT_FRIENDLY;
        // else, we are moving in diagonal so we can capture
        return e_Target.CAPTURE_MOVE;            
    }
    
    @Override
    public char getLetter() { return 'P';  }
    
    @Override
    public boolean isLongRange() { return false; }
    
    // </editor-fold>

}
