
package games.chess.chesspieces;

import games.chess.LogicChessBoard;
import games.chess.MovesList;

/**
 * class PieceQueen
 * the Queen can move like a rook and like a bishop,
 * but since its not C++ and we dont have multiple inheritance,
 * i simply use the very generic incrementToLastMove to generate the moves needed.
 * @author Niv B.
 */
public class PieceQueen extends ChessPiece {

    // <editor-fold desc="ctor">
    
    /*
     * ctor creates a new queen piece
     */
    public PieceQueen(boolean is_white, int i, int j) {
        super(is_white,i,j);
        img = getImagePiece("queen", is_white);
    }

    
    // </editor-fold>


    // <editor-fold desc="get/set">
    
    
    
    // </editor-fold>

    /***************************************************************************/

    // <editor-fold desc="class methods">
    
    @Override
    public MovesList generateMoves(LogicChessBoard b) {
        MovesList possible_moves = new MovesList(b.getMovesCounter());
        possible_moves.addAll(incrementToLastMove(b, 1,1));
        possible_moves.addAll(incrementToLastMove(b, 1,0));
        possible_moves.addAll(incrementToLastMove(b, 1,-1));
        possible_moves.addAll(incrementToLastMove(b, -1,1));
        possible_moves.addAll(incrementToLastMove(b, -1,0));
        possible_moves.addAll(incrementToLastMove(b, -1,-1));
        possible_moves.addAll(incrementToLastMove(b, 0,1));
        possible_moves.addAll(incrementToLastMove(b, 0,-1));
        return possible_moves;
    }
    
    @Override
    protected boolean isLegalSquare(int xto, int yto) {
        int dx = xto-x;
        int dy = yto-y;
        if(dx*dx == dy*dy) // check diagonal
            return true;
        if(dx==0 || dy==0) // check horizontal/vertical
            return true;
        return false;
    }
    
    @Override
    public e_Target isTargetBlocked(LogicChessBoard b, int xto, int yto) {
        ChessPiece cp;
        int dx = xto-x;
        int dy = yto-y;
        int pdx = dx==0 ? 0 : dx/Math.abs(dx); // is one of +1,0,-1 to indicate one movement to advance to target cell
        int pdy = dy==0 ? 0 : dy/Math.abs(dy);
        
        for(int cur_x = x+pdx, cur_y = y+pdy ;
                cur_x!=xto || cur_y!=yto ;  // while we didnt reach (ito,jto)
                cur_x += pdx, cur_y += pdy) {
            // looping while we didnt reach destination, not including the check on the destination cell because it has different outcomes to return
            // moveing each cell from trajcetory of (ifrom,jfrom)-->(ito,jto) , checking if someone on the way
            //System.out.println("checking "+cur_x+","+cur_y);
            cp = b.getPieceChess(cur_x, cur_y);
            if(cp!=null) { // means we are blocked
                if(this.isFriendly(cp))
                    return e_Target.BLOCKED_FRIENDLY;
                return e_Target.BLOCKED_NOT_FRIENDLY; 
            }
        }
        // we get here if we checked all the way and didnt find anything along the way to destination
        // now check the destination
        cp = b.getPieceChess(xto, yto);
        if(cp==null)
            return e_Target.OPEN;
        if(this.isFriendly(cp))
            return e_Target.BLOCKED_FRIENDLY;
        return e_Target.CAPTURE_MOVE;            
    }
    
    
    @Override
    public char getLetter() { return 'Q';  }
    
    @Override
    public boolean isLongRange() { return true; }
    
    // </editor-fold>


    // <editor-fold desc="private methods">
    
    
    
    // </editor-fold>

}
